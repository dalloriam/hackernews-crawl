# -*- coding: utf-8 -*-
from scrapy import Request, Spider
from news.items import NewsItem

from lxml import html, etree

import logging

class HackernewsSpider(Spider):
    name = "hackernews"
    allowed_domains = ["ycombinator.com", "news.ycombinator.com"]
    start_urls = (
        'https://news.ycombinator.com/news?p={0}',
    )
    base_url = "https://news.ycombinator.com/"
    count = 0
    current_page = 1

    def parse(self, response):

      # On recupere la "page" en HTML retournee par la derniere requete
      raw_body = response.body_as_unicode()
      xml = html.fromstring(raw_body)

      # On sélectionne tous les URLs d'articles
      results = xml.xpath('//td[@class="subtext"]/a[contains(@href, "item")]/@href')

      # Yield = Comme "return" pour une fonction retournant plusieurs fois
      # Donc, pour chaque article de la page, retourne une requete vers l'article
      for result in results:
        yield Request(url=(self.base_url + result), callback=self.parse_comments)

      # On va chercher la prochaine page
      self.current_page += 1
      yield Request(url=self.start_urls[0].format(self.current_page), callback=self.parse)

    def parse_comments(self, response):
      raw_body = response.body_as_unicode()
      xml = html.fromstring(raw_body)

      title = str(xml.xpath('//td[@class="title"]/a/text()')[0])
      link = str(xml.xpath('//td[@class="title"]/a/@href')[0])
      comment_objs_list = xml.xpath('//span[@class="c00"]')


      # Equivalent a:
      # comments = []
      # for comment in comment_objs_list:
      #   comments.append(etree.tostring(comment))
      comments = map(lambda x: etree.tostring(x), comment_objs_list)

      # Création de l'objet NewsItem
      article = NewsItem()
      article["title"] = title
      article["link"] = link
      article["comments"] = comments
      article["number"] = str(self.count) # Pour la base de données (Index ne peut pas être un int :/)


      self.count += 1
      logging.info("NOMBRE D'ARTICLES: " + str(self.count))

      yield article
