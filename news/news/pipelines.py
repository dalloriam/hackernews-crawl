# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import shelve
import os
import json


class NewsPipeline(object):
    def process_item(self, item, spider):
        return item

class ShelveWriterPipeline(object):

  def __init__(self):
    self.data = {}

  def save(self):
    if os.path.exists("data.json"):
      os.remove("data.json")

    with open('data.json', 'a') as outfile:
      outfile.write(json.dumps(self.data))

  def process_item(self, item, spider):
    self.data[item['number']] = dict(item)
    self.save()
    return item
